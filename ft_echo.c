/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_echo.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 15:36:30 by jwong             #+#    #+#             */
/*   Updated: 2016/06/27 17:16:14 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"
#include "ft_printf.h"
#include "minishell.h"

static void	ft_getvalue(char **environ, char *s)
{
	char	*value;
	int		i;
	int		j;

	i = 0;
	while (environ[i] && ft_strncmp(environ[i], &s[1], ft_strlen(&s[1])) != 0)
		i++;
	if (environ[i])
	{
		value = ft_strdup(environ[i]);
		j = 0;
		while (value[j] && value[j] != '=')
			j++;
		j++;
		ft_printf("%s\n", &value[j]);
		free(value);
	}
	else
		write(1, "\n", 1);
}

void		ft_echo(t_com *com, char **environ)
{
	char	*tmp;
	int		tok;

	tok = 0;
	if ((*com).path[0] == '$')
		tok = 1;
	tmp = (char *)malloc(sizeof(*tmp) * ft_strlen((*com).path));
	if (tmp)
		ft_trim_quotes((*com).path, tmp);
	if (tmp[0] == '$' && tok == 1)
		ft_getvalue(environ, tmp);
	else
		ft_printf("%s\n", tmp);
	free(tmp);
}
