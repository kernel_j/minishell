/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getline.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 18:29:01 by jwong             #+#    #+#             */
/*   Updated: 2016/06/16 11:16:51 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdlib.h>
#include "ft_printf.h"
#include "libft.h"
#include "minishell.h"

static int		ft_process_buffer(char *buff, char **line)
{
	char	*tmp;
	int		i;

	i = 0;
	while (buff[i])
	{
		if (buff[i] == '\n')
		{
			buff[i] = '\0';
			tmp = *line;
			*line = ft_strjoin(tmp, buff);
			free(tmp);
			return (-1);
		}
		i++;
	}
	tmp = *line;
	*line = ft_strjoin(*line, buff);
	free(tmp);
	return (0);
}

void			ft_getline(char **line)
{
	char	buff[BUFF + 1];
	int		bytes_read;

	bytes_read = 42;
	while ((bytes_read = read(0, buff, BUFF)) > 0)
	{
		buff[bytes_read] = '\0';
		if (ft_process_buffer(buff, line) == -1)
			break ;
	}
	if (bytes_read == 0)
		exit(1);
}
