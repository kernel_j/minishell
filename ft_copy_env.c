/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_copy_env.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/13 15:06:29 by jwong             #+#    #+#             */
/*   Updated: 2016/06/13 15:06:33 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "minishell.h"

char	**ft_copy_env(char **env, int size)
{
	char	**environ;
	int		i;

	environ = (char **)malloc(sizeof(char *) * (size + 1));
	if (!environ)
	{
		ft_error_msg("malloc failure", NULL);
		exit(1);
	}
	i = 0;
	while (*env)
	{
		environ[i] = ft_strdup(*env);
		i++;
		env++;
	}
	environ[i] = NULL;
	return (environ);
}
