/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/30 17:47:15 by jwong             #+#    #+#             */
/*   Updated: 2016/06/27 13:50:53 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "libft.h"
#include "minishell.h"

static int	ft_check_tab(char **tab)
{
	int	i;

	if (tab == NULL || *tab == NULL)
		return (-1);
	i = 0;
	while (tab[i])
	{
		if (tab[i][0] != '\0')
			return (0);
		i++;
	}
	ft_clean_env(tab);
	return (-1);
}

static char	*ft_get_com(char **tab)
{
	int		i;
	char	*tmp;

	i = 0;
	while (tab[i])
	{
		if (tab[i][0] != '\0')
		{
			tmp = ft_strdup(tab[i]);
			tab[i][0] = '\0';
			return (tmp);
		}
		i++;
	}
	return (ft_strdup(""));
}

static int	ft_minishell(t_com *com, char ***environ, char **tab)
{
	char	*line;
	char	*tmp;

	if (tab == NULL || ft_check_tab(tab) == -1)
	{
		line = ft_strdup("");
		write(1, "d[o_0]b o}==> ", 14);
		ft_getline(&line);
		if ((tab = ft_parse_commands(line)) == NULL)
			return (-1);
		free(line);
	}
	while (ft_check_tab(tab) != -1)
	{
		tmp = ft_get_com(tab);
		ft_parseline(com, tmp);
		ft_command(com, environ);
		ft_shell_clean(com);
		free(tmp);
	}
	return (0);
}

int			main(int argc, char **argv, char **envp)
{
	t_com	com;
	char	**environ;
	char	**tab;

	if (argc != 1 || *argv == NULL)
		return (1);
	tab = NULL;
	environ = ft_copy_env(envp, ft_count_env(envp));
	ft_bzero(&com, sizeof(com));
	environ = ft_set_prevpwd(environ);
	while (1)
	{
		if (ft_minishell(&com, &environ, tab) == -1)
			return (-1);
	}
	return (0);
}
