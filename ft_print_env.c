/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_env.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 19:46:59 by jwong             #+#    #+#             */
/*   Updated: 2016/06/10 19:47:05 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_print_env(char **environ)
{
	char **env;

	if (!environ || !(*environ))
		return ;
	env = environ;
	while (*env != 0)
	{
		ft_printf("%s\n", *env);
		env++;
	}
}
