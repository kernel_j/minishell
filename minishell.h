/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/31 14:45:47 by jwong             #+#    #+#             */
/*   Updated: 2016/07/19 12:11:12 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# define BUFF 45
# define MAX 4096

typedef struct	s_com
{
	char	**dir;
	char	*command;
	char	*path;
}				t_com;

/*
**	ft_clean_env.c
*/
void			ft_clean_env(char **environ);
void			ft_shell_clean(t_com *com);

/*
**	ft_env_misc.c
*/
int				ft_checkenv_var(char *s);
int				ft_count_env(char **env);
char			*ft_getenv_name(char *s);
char			*ft_getenv_var(char **environ, char *s);
int				ft_numtoadd(char **value);

/*
**	ft_error_msg.c
*/
void			ft_error_msg(char *msg, char *s);
void			ft_error_msg2(char *msg, char *msg2, char *s);

void			ft_cd(t_com *com, char ***environ);
void			ft_command(t_com *com, char ***environ);
char			**ft_copy_env(char **env, int size);
void			ft_echo(t_com *com, char **environ);
int				ft_executables(t_com *com, char **environ);
int				ft_execute(char **args, char *command, char **environ);
void			ft_exit(t_com *com);
void			ft_getline(char **line);
char			**ft_parse_commands(char *s);
void			ft_parseline(t_com *com, char *line);
void			ft_print_env(char **environ);
char			**ft_set_prevpwd(char **environ);
char			**ft_setenv(char **environ, char *name);
char			*ft_strtrim_it(char *s);
void			ft_trim_quotes(char *s, char *tmp);
char			**ft_unsetenv(char **environ, char *name);

#endif
