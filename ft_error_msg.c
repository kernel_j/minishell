/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error_msg.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 19:25:01 by jwong             #+#    #+#             */
/*   Updated: 2016/06/14 13:34:27 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "ft_printf.h"
#include "libft.h"
#include "minishell.h"

void	ft_error_msg(char *msg, char *s)
{
	int		len;
	char	*tmp;

	if (!s)
		len = ft_strlen(msg) + 2;
	else
		len = ft_strlen(msg) + ft_strlen(s) + 2;
	tmp = (char *)malloc(sizeof(*tmp) * len);
	if (tmp)
	{
		if (!s)
			ft_snprintf(tmp, len, "%s\n", msg);
		else
			ft_snprintf(tmp, len, "%s%s\n", msg, s);
		write(2, tmp, len - 1);
		free(tmp);
	}
}

void	ft_error_msg2(char *msg, char *msg2, char *s)
{
	int		len;
	char	*tmp;

	len = ft_strlen(msg) + ft_strlen(msg2) + ft_strlen(s) + 2;
	tmp = (char *)malloc(sizeof(tmp) * len);
	if (tmp)
	{
		ft_snprintf(tmp, len, "%s%s%s\n", msg, s, msg2);
		write(2, tmp, len - 1);
		free(tmp);
	}
}
