/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env_misc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 20:24:30 by jwong             #+#    #+#             */
/*   Updated: 2016/06/14 14:04:25 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"

int		ft_numtoadd(char **value)
{
	int	total;
	int	i;

	total = 0;
	i = 0;
	while (value[i])
	{
		if (value[i][0] != '\0')
			total++;
		i++;
	}
	return (total);
}

int		ft_checkenv_var(char *s)
{
	int	i;

	i = 0;
	while (s[i] && s[i] != '=')
		i++;
	if (s[i] == '=')
		return (i);
	return (-1);
}

char	*ft_getenv_name(char *s)
{
	char	*name;
	int		len;

	name = NULL;
	if ((len = ft_checkenv_var(s)) > 0)
		name = ft_strsub(s, 0, len);
	return (name);
}

int		ft_count_env(char **env)
{
	int total;

	total = 0;
	while (*env)
	{
		env++;
		total++;
	}
	return (total);
}

char	*ft_getenv_var(char **environ, char *s)
{
	char	*var;
	char	**tmp;
	int		len;
	int		i;

	tmp = environ;
	len = ft_strlen(s);
	while (*tmp && ft_strncmp(*tmp, s, len) != 0)
		tmp++;
	if (!(*tmp))
		return (NULL);
	i = 0;
	while ((*tmp)[i] && (*tmp)[i] != '=')
		i++;
	var = ft_strdup(*tmp + (i + 1));
	return (var);
}
