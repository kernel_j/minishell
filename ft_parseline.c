/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parseline.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 18:33:23 by jwong             #+#    #+#             */
/*   Updated: 2016/06/27 16:10:41 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "minishell.h"

static void	ft_parsespace(t_com *com)
{
	char	*tmp;

	if ((*com).path != NULL && (*com).path[0] != '\0')
	{
		tmp = (*com).path;
		(*com).path = ft_strtrim_it((*com).path);
		free(tmp);
	}
}

void		ft_parseline(t_com *com, char *line)
{
	int	i;
	int	j;

	i = 0;
	while (line[i] && (line[i] == ' ' || line[i] == '\t'))
		i++;
	j = i;
	while (line[j] && line[j] != ' ' && line[j] != '\t')
		j++;
	(*com).command = ft_strsub(line, i, (j - i));
	while (line[j] && (line[j] == ' ' || line[j] == '\t'))
		j++;
	i = j;
	while (line[i])
		i++;
	(*com).path = ft_strsub(line, j, (i - j));
	if (ft_strcmp((*com).command, "echo") != 0)
		ft_parsespace(com);
}
