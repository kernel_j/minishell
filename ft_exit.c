/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 11:57:57 by jwong             #+#    #+#             */
/*   Updated: 2016/07/19 14:30:28 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "minishell.h"

static int		ft_checkarg(char *s, char **err)
{
	int		i;
	char	*tmp;

	i = 0;
	while (s[i] && s[i] != ' ')
	{
		if (ft_isdigit((int)s[i]) == 0)
			break ;
		i++;
	}
	if (s[i] != '\0' && s[i] != ' ')
	{
		if ((tmp = (char *)malloc(sizeof(*tmp) * (i + 1))))
		{
			*err = ft_strncpy(tmp, s, i);
			return (-1);
		}
	}
	else if (s[i] == '\0')
		return (0);
	else if (s[i] == ' ' && s[i + 1] != '\0')
		return (-2);
	return (-3);
}

void			ft_exit(t_com *com)
{
	int		ret;
	char	*err;

	if (!(*com).path || (*com).path[0] == '\0')
		exit(0);
	if ((ret = ft_checkarg((*com).path, &err)) == -1)
	{
		ft_error_msg2("minishell: exit: ", ": numberic argument required", err);
		free(err);
	}
	else if (ret == -2)
	{
		ft_error_msg("minishell: exit: too many arguments", NULL);
		exit(255);
	}
	else if (ret == 0)
		exit(ft_atoi((*com).path));
}
