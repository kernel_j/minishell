/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim_it.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/23 14:01:36 by jwong             #+#    #+#             */
/*   Updated: 2016/06/23 14:04:41 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "minishell.h"

static char	*ft_trim_it(char *s, char *new)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (s[i] && (s[i] == ' ' || s[i] == '\t'))
		i++;
	while (s[i])
	{
		if (s[i] != ' ' && s[i] != '\t')
			new[j++] = s[i];
		else if (s[i] == ' ' || s[i] == '\t')
		{
			if (s[i + 1] && s[i + 1] != ' ' && s[i + 1] != '\t')
				new[j++] = ' ';
		}
		i++;
	}
	new[j] = '\0';
	return (new);
}

char		*ft_strtrim_it(char *s)
{
	char	*new;
	int		len;

	if (s == NULL || s[0] == '\0')
		return (s);
	len = ft_strlen(s) + 1;
	new = (char *)malloc(sizeof(*new) * len);
	if (new)
		return (ft_trim_it(s, new));
	return (new);
}
