/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_execute.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/16 11:30:25 by jwong             #+#    #+#             */
/*   Updated: 2016/06/16 15:11:43 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <sys/wait.h>
#include "minishell.h"

int	ft_execute(char **args, char *command, char **environ)
{
	pid_t	parent;

	if (access(command, X_OK) == -1)
	{
		ft_error_msg("minishell: permission denied: ", args[0]);
		return (1);
	}
	parent = fork();
	if (parent > 0)
		wait(0);
	if (parent == 0)
		execve(command, args, environ);
	return (0);
}
