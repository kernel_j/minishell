# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jwong <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/04/20 16:18:45 by jwong             #+#    #+#              #
#    Updated: 2016/07/19 12:11:31 by jwong            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= minishell

SRC		= main.c				\
		  ft_cd.c				\
		  ft_clean_env.c		\
		  ft_command.c			\
		  ft_copy_env.c			\
		  ft_echo.c				\
		  ft_env_misc.c			\
		  ft_error_msg.c		\
		  ft_executables.c		\
		  ft_execute.c			\
		  ft_exit.c				\
		  ft_getline.c			\
		  ft_parse_commands.c	\
		  ft_parseline.c		\
		  ft_print_env.c		\
		  ft_set_prevpwd.c		\
		  ft_setenv.c			\
		  ft_strtrim_it.c		\
		  ft_trim_quotes.c		\
		  ft_unsetenv.c		

CC		= clang
CFLAGS	= -Wall -Werror -Wextra -I libft/includes
OBJ		= $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
		make -C libft/
		$(CC) $(CFLAGS) $(OBJ) -o $(NAME) -L libft/ -lft

%.o: %.c
		$(CC) $(CFLAGS) $< -c -o $@

clean:
		make -C libft/ clean
		rm -f $(OBJ)

fclean: clean
		make -C libft/ fclean
		rm -f $(NAME)

re: fclean all
