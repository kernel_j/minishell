/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_command.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/13 14:57:15 by jwong             #+#    #+#             */
/*   Updated: 2016/07/19 12:14:11 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "minishell.h"

static void	ft_preprocess(t_com *com)
{
	char	*tmp;
	char	*tmp2;

	if ((*com).command[0] == '\0'
			|| (ft_strchr((*com).command, (int)'\'') == NULL
				&& ft_strchr((*com).command, (int)'"') == NULL))
		return ;
	tmp = (char *)malloc(sizeof(*tmp) * (ft_strlen((*com).command) + 1));
	if (tmp)
	{
		ft_trim_quotes((*com).command, tmp);
		tmp2 = (*com).command;
		(*com).command = ft_strdup(tmp);
		free(tmp);
		free(tmp2);
	}
}

void		ft_command(t_com *com, char ***environ)
{
	ft_preprocess(com);
	if ((*com).command[0] == '\0')
		return ;
	if (ft_strcmp((*com).command, "cd") == 0)
	{
		if ((*com).path[0] != '-')
			*environ = ft_set_prevpwd(*environ);
		ft_cd(com, environ);
	}
	else if (ft_strcmp((*com).command, "echo") == 0)
		ft_echo(com, *environ);
	else if (ft_strcmp((*com).command, "setenv") == 0)
		*environ = ft_setenv(*environ, (*com).path);
	else if (ft_strcmp((*com).command, "unsetenv") == 0)
		*environ = ft_unsetenv(*environ, (*com).path);
	else if (ft_strcmp((*com).command, "env") == 0)
		ft_print_env(*environ);
	else if (ft_strcmp((*com).command, "exit") == 0)
		ft_exit(com);
	else
		ft_executables(com, *environ);
}
