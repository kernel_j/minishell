/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_executables.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 19:25:30 by jwong             #+#    #+#             */
/*   Updated: 2016/06/29 19:14:23 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <dirent.h>
#include <stdlib.h>
#include "libft.h"
#include "minishell.h"

static int		ft_isvalid_command(char *command, char *dir_path)
{
	struct dirent	*dp;
	DIR				*dirp;

	if (ft_strchr(command, '/') != NULL || command[0] == '.')
		return (1);
	dirp = opendir(dir_path);
	if (dirp == NULL)
		return (-1);
	while ((dp = readdir(dirp)) != NULL)
	{
		if (ft_strcmp((*dp).d_name, command) == 0)
		{
			closedir(dirp);
			return (0);
		}
	}
	closedir(dirp);
	return (-1);
}

static char		*ft_join(t_com *com, int i, int ret)
{
	char	*tmp;
	char	*tmp2;
	char	*command;

	if (ret == 0)
	{
		command = ft_strjoin((*com).dir[i], "/");
		tmp = command;
		command = ft_strjoin(command, (*com).command);
		free(tmp);
	}
	else
		command = ft_strdup((*com).command);
	tmp2 = ft_strjoin((*com).command, " ");
	tmp = (*com).path;
	(*com).path = ft_strjoin(tmp2, (*com).path);
	free(tmp);
	free(tmp2);
	return (command);
}

static int		ft_check_path(t_com *com, char **environ, int *i)
{
	char	*tmp;
	int		ret;

	tmp = ft_getenv_var(environ, "PATH");
	if (tmp == NULL)
	{
		if (ft_strchr((*com).command, (int)'/') != NULL
				|| (*com).command[0] == '.')
			return (2);
		return (-2);
	}
	(*com).dir = ft_strsplit(tmp, ':');
	while ((*com).dir[(*i)]
			&& (ret = ft_isvalid_command((*com).command,\
					(*com).dir[(*i)])) == -1)
		(*i)++;
	free(tmp);
	return (ret);
}

int				ft_executables(t_com *com, char **environ)
{
	char		**args;
	char		*command;
	int			i;
	int			ret;

	i = 0;
	ret = ft_check_path(com, environ, &i);
	if (ret == 0 || ret == 1 || ret == 2)
	{
		command = ft_join(com, i, ret);
		args = ft_strsplit((*com).path, ' ');
		ret = ft_execute(args, command, environ);
		if (ret != 2)
			ft_clean_env((*com).dir);
		ft_clean_env(args);
		free(command);
		return (ret);
	}
	else
	{
		if (ret != -2)
			ft_clean_env((*com).dir);
		ft_error_msg("minishell: command not found: ", (*com).command);
	}
	return (1);
}
