/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_clean_env.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 20:19:17 by jwong             #+#    #+#             */
/*   Updated: 2016/06/29 19:14:45 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "minishell.h"

void	ft_shell_clean(t_com *com)
{
	if ((*com).command)
		free((*com).command);
	if ((*com).path)
		free((*com).path);
}

void	ft_clean_env(char **environ)
{
	char **tmp;

	if (!environ)
		return ;
	tmp = environ;
	while (environ && *environ)
	{
		free(*environ);
		environ++;
	}
	free(tmp);
}
