/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse_commands.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/27 13:51:17 by jwong             #+#    #+#             */
/*   Updated: 2016/06/27 13:55:05 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "libft.h"

static size_t	ft_count_commands(char *s)
{
	char	c;
	size_t	i;
	size_t	command;

	i = 0;
	command = 0;
	while (s[i])
	{
		if (s[i] == '\'' || s[i] == '"')
		{
			c = s[i++];
			while (s[i] && s[i] != c)
				i++;
		}
		else if (s[i] == ';')
			command++;
		i++;
	}
	return (command + 1);
}

static size_t	ft_command_len(char *s)
{
	char	c;
	size_t	len;

	len = 0;
	while (*s && (*s == ' ' || *s == '\t'))
		s++;
	while (*s)
	{
		if (*s == ';')
			break ;
		else if (*s == '\'' || *s == '"')
		{
			c = *s;
			len++;
			while (*(++s) && *s != c)
				len++;
			len++;
		}
		else
			len++;
		s++;
	}
	return (len);
}

static size_t	ft_quotes(char *s, char *new, size_t *j, size_t i)
{
	char	c;

	c = s[i];
	new[(*j)++] = s[i];
	s[i] = ' ';
	while (s[++i] && s[i] != '\'' && s[i] != '"')
	{
		new[(*j)++] = s[i];
		s[i] = ' ';
	}
	if (s[i])
	{
		new[(*j)++] = s[i];
		s[i] = ' ';
	}
	return (i);
}

static char		*ft_command_cpy(char *s, char *command)
{
	size_t	j;
	size_t	i;

	j = 0;
	i = 0;
	while (s[i] && (s[i] == ' ' || s[i] == '\t'))
		i++;
	while (s[i])
	{
		if (s[i] == ';')
		{
			s[i] = ' ';
			break ;
		}
		else if (s[i] == '\'' || s[i] == '"')
			i = ft_quotes(s, command, &j, i);
		else
		{
			command[j++] = s[i];
			s[i] = ' ';
		}
		i++;
	}
	command[j] = '\0';
	return (command);
}

char			**ft_parse_commands(char *s)
{
	char	**tab;
	char	*tmp;
	size_t	commands;
	size_t	count;
	size_t	len;

	if (!s)
		return (NULL);
	commands = ft_count_commands(s) + 1;
	if ((tab = (char **)malloc(sizeof(char *) * commands)))
	{
		count = 0;
		while (count < (commands - 1))
		{
			len = ft_command_len(s) + 1;
			if (!(tmp = (char *)malloc(sizeof(*tmp) * len)))
				return (NULL);
			tab[count] = ft_command_cpy(s, tmp);
			count++;
		}
		tab[count] = NULL;
	}
	return (tab);
}
