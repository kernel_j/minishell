/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unsetenv.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 20:37:30 by jwong             #+#    #+#             */
/*   Updated: 2016/06/10 20:38:29 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_printf.h"
#include "libft.h"
#include "minishell.h"

static int		ft_checkenv_name(char *s)
{
	int	i;

	i = 0;
	while (s[i])
	{
		if (s[i] == '=')
		{
			ft_error_msg2("minishell: unsetenv '",\
			"': not a valid identifier", s);
			return (-1);
		}
		i++;
	}
	return (0);
}

static char		**ft_remove(char **environ, int diff)
{
	char	**new;
	int		i;
	int		j;

	new = (char **)malloc(sizeof(char *) * (diff + 1));
	if (new)
	{
		i = 0;
		j = 0;
		while (environ[i])
		{
			if (environ[i][0] != '\0')
				new[j++] = ft_strdup(environ[i]);
			i++;
		}
		new[j] = NULL;
	}
	return (new);
}

static void		ft_process(char **environ, char **names)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (names[j])
	{
		if (ft_checkenv_name(names[j]) == 0)
		{
			i = 0;
			while (environ[i])
			{
				if (ft_strncmp(names[j], environ[i], ft_strlen(names[j])) == 0)
					environ[i][0] = '\0';
				i++;
			}
		}
		j++;
	}
}

char			**ft_unsetenv(char **environ, char *name)
{
	char	**tmp;
	char	**names;

	if (!name || !environ || !(*environ))
		return (environ);
	names = ft_strsplit(name, ' ');
	ft_process(environ, names);
	ft_clean_env(names);
	tmp = environ;
	environ = ft_remove(environ, ft_count_env(environ));
	ft_clean_env(tmp);
	return (environ);
}
