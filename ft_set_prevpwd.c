/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_set_prevpwd.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/16 11:57:03 by jwong             #+#    #+#             */
/*   Updated: 2016/06/23 14:37:17 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "libft.h"
#include "minishell.h"

char	**ft_set_prevpwd(char **environ)
{
	char	buff[MAX];
	char	**env;
	char	*tmp;
	char	*new_env;

	if ((tmp = getcwd(buff, MAX)) != NULL)
	{
		new_env = ft_strjoin("PREV_PWD=", tmp);
		env = ft_setenv(environ, new_env);
		free(new_env);
		return (env);
	}
	return (environ);
}
