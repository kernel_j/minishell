/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cd.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/13 14:52:06 by jwong             #+#    #+#             */
/*   Updated: 2016/06/23 16:53:51 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include "ft_printf.h"
#include "libft.h"
#include "minishell.h"

static int		ft_access(char *path)
{
	int	ret;

	ret = access(path, X_OK);
	return (ret);
}

static int		ft_isvalid_path(char *path, char *name)
{
	struct stat		sb;
	unsigned int	mode;

	if (lstat(path, &sb) == -1)
		return (0);
	mode = sb.st_mode & S_IFMT;
	if (mode != S_IFDIR && mode != S_IFLNK)
	{
		ft_error_msg("cd: not a directory: ", name);
		return (1);
	}
	if (ft_access(path) == -1)
	{
		ft_error_msg("cd: permission denied: ", name);
		return (1);
	}
	return (0);
}

static void		ft_home(t_com *com, char **environ)
{
	char	*directory;
	char	*tmp;

	if ((directory = ft_getenv_var(environ, "HOME")) == NULL)
		directory = ft_strdup("./");
	if ((*com).path[0] == '~')
	{
		tmp = (*com).path;
		(*com).path = ft_strjoin(directory, &(*com).path[1]);
		free(tmp);
	}
	else
	{
		tmp = (*com).path;
		(*com).path = ft_strdup(directory);
		free(tmp);
	}
	free(directory);
	tmp = (*com).path;
	(*com).path = ft_strtrim((*com).path);
	free(tmp);
	if (chdir((*com).path) == -1)
		ft_error_msg("cd: no such file or directory: ", (*com).path);
}

static void		ft_previous(t_com *com, char ***environ)
{
	char	*directory;

	if ((directory = ft_getenv_var(*environ, "PREV_PWD")) == NULL)
	{
		ft_error_msg("minishell: cd: PREV_PWD not set", NULL);
		return ;
	}
	*environ = ft_set_prevpwd(*environ);
	if (chdir(directory) == -1)
		ft_error_msg("cd: no such file or directory: ", (*com).path);
	else
		ft_printf("%s\n", directory);
	free(directory);
}

void			ft_cd(t_com *com, char ***environ)
{
	char	*tmp;
	char	*tmp2;

	if ((*com).path[0] == '\0' || (*com).path[0] == '~')
		ft_home(com, *environ);
	else if ((*com).path[0] == '-')
		ft_previous(com, environ);
	else
	{
		tmp = ft_strtrim((*com).path);
		if (strcmp(tmp, "..") != 0 && ft_strcmp(tmp, ".") != 0 && tmp[0] != '/')
		{
			tmp2 = tmp;
			tmp = ft_strjoin("./", tmp);
			free(tmp2);
		}
		if (ft_isvalid_path(tmp, (*com).path) == 0)
		{
			if (chdir(tmp) == -1)
				ft_error_msg("cd: no such file or directory: ", (*com).path);
		}
		free(tmp);
	}
}
