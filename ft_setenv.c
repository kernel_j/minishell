/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/10 20:20:14 by jwong             #+#    #+#             */
/*   Updated: 2016/06/10 20:24:16 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "minishell.h"

static void		ft_check_value(char **value)
{
	int	i;

	i = 0;
	while (value[i])
	{
		if (ft_checkenv_var(value[i]) == -1)
			value[i][0] = '\0';
		i++;
	}
}

static void		ft_process_env(char **environ, char **value)
{
	char	*var_name;
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (value[i])
	{
		if ((var_name = ft_getenv_name(value[i])) != NULL)
		{
			j = -1;
			while (environ[++j])
			{
				if (ft_strncmp(environ[j], var_name, ft_strlen(var_name)) == 0)
				{
					free(environ[j]);
					environ[j] = ft_strdup(value[i]);
					value[i][0] = '\0';
					break ;
				}
			}
			free(var_name);
		}
		i++;
	}
}

static char		**ft_add_envvar(char **environ, char **value)
{
	char	**new;
	int		i;
	int		j;
	int		total;

	total = ft_numtoadd(value) + ft_count_env(environ) + 2;
	new = (char **)malloc(sizeof(char *) * total);
	if (new)
	{
		i = -1;
		j = 0;
		while (++i < (ft_count_env(environ) - 1) && environ[i])
			new[j++] = ft_strdup(environ[i]);
		i = -1;
		while (value[++i])
		{
			if (value[i][0] != '\0')
				new[j++] = ft_strdup(value[i]);
		}
		new[j++] = ft_strdup(environ[ft_count_env(environ) - 1]);
		new[j] = NULL;
	}
	return (new);
}

char			**ft_setenv(char **environ, char *name)
{
	char	**value;
	char	**tmp;

	if (!name || !environ || !(*environ))
		return (environ);
	value = ft_strsplit(name, ' ');
	ft_check_value(value);
	ft_process_env(environ, value);
	tmp = environ;
	environ = ft_add_envvar(environ, value);
	ft_clean_env(tmp);
	ft_clean_env(value);
	return (environ);
}
